extern crate matplotrust;

use matplotrust::*;

fn main() {
    let x = vec![1, 2, 3, 4];
    let y = vec![0.1, 0.2, 0.5, 0.3];
    let lp = line_plot::<i32, f64>(x, y, None);
    let mut figure = Figure::new();
    figure.add_plot(lp.clone());
    figure.add_plot(lp.clone());
    print!("{:?}", figure.save("./figures/lineplot_basic.png", None));
}