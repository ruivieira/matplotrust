# matplotrust examples

## line plots

Line plots are generated with `line_plot::<T, U>()`.
Below is an example:

```rust
extern crate matplotrust;

use matplotrust::*;

fn main() {
    let x = vec![1, 2, 3, 4];
    let y = vec![0.1, 0.2, 0.5, 0.3];
    let lp = line_plot::<i32, f64>(x, y, None);
    let mut figure = Figure::new();
    figure.add_plot(lp.clone());
    figure.add_plot(lp.clone());
    print!("{:?}", figure.save("./examples/figures/lineplot_basic.png"));
}
```

which will generate

![simple scatter](figures/lineplot_basic.png)

## histograms

Histograms can be created with `histogram::<U>()`. For instance:

```rust
extern crate matplotrust;

use matplotrust::*;

fn main() {
         let mut source = source::default();
         let gaussian = Gaussian::new(0.0, 2.0);
         let mut sampler = Independent(&gaussian, &mut source);
         let x = sampler.take(500).collect::<Vec<_>>();
         let plot = histogram::<f64>(x, None);
         let mut figure = Figure::new();
         figure.add_plot(plot.clone());
         print!("{:?}", figure.save("./examples/figures/histogram_default_bins.png", None));
}
```

will generate

![simple scatter](figures/histogram_default_bins.png)

The number of bins can be configured with

```rust
let plot = histogram::<f64>(x, 100);
```

which in turn will generate

![simple scatter](figures/histogram_custom_bins.png)

## vertical, horizontal lines

Vertical and horizontal lines can be added with `vertical_line` and `horizontal_line` respectively.
An example:

```rust
let mut source = source::default();
let gaussian = Gaussian::new(0.0, 2.0);
let mut sampler = Independent(&gaussian, &mut source);
let x = sampler.take(500).collect::<Vec<_>>();
let plot = histogram::<f64>(x.clone(), Some(100));
let mut figure = Figure::new();
figure.add_plot(plot);
let mean = mean(&(x.clone()));
let mut mean_line_opts = LinePlotOptions::new();
mean_line_opts.lineStyle = Some(Dash);
mean_line_opts.colour = Some("black".to_string());
let mean_line = vertical_line(mean, Some(mean_line_opts));
figure.add_plot(mean_line);
figure.save("./examples/figures/histogram_mean_line.png", None);
```

![plot](figures/histogram_mean_line.png)

```rust
let mut source = source::default();
let gaussian = Gaussian::new(0.0, 2.0);
let mut sampler = Independent(&gaussian, &mut source);
let x = (0..500).collect();
let y = sampler.take(500).collect::<Vec<_>>();
let plot = scatter_plot::<i32, f64>(x, y.clone(), None);
let mut figure = Figure::new();
figure.add_plot(plot);
let mean = mean(&(y.clone()));
let mut mean_line_opts = LinePlotOptions::new();
mean_line_opts.lineStyle = Some(Dash);
mean_line_opts.colour = Some("black".to_string());
let mean_line = horizontal_line(mean, Some(mean_line_opts));
figure.add_plot(mean_line);
figure.save("./examples/figures/horizontal_line.png", None);
```

![plot](figures/horizontal_line.png)

## line segments

```rust
let x = vec![1, 2, 3, 4];
let y = vec![0.1, 0.2, 0.5, 0.3];
let mut options = ScatterPlotOptions::new();
options.marker = Some(Markers::Diamond);
let lp = scatter_plot::<i32, f64>(x, y, Some(options));
let mut figure = Figure::new();
figure.add_plot(lp);
let mut line_opt = LinePlotOptions::new();
line_opt.colour = Some("red".to_string());
line_opt.lineStyle = Some(LineStyle::Dash);
figure.add_plot(line::<i32, f64>((1, 0.1), (4, 0.3), Some(line_opt)));
```

![plot](figures/line_segment.png)