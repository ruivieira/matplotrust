extern crate matplotrust;

use matplotrust::*;

fn main() {
    let x = vec![1, 2, 3, 4];
    let y = vec![0.1, 0.2, 0.5, 0.3];
    let mut options = LinePlotOptions::new();
    options.marker = Some(Markers::Diamond);
    let lp = line_plot::<i32, f64>(x, y, Some(options));
    // using points
    let mut figure = Figure::new();
    figure.add_plot(lp.clone());
    figure.add_plot(lp.clone());
    print!("{:?}", figure.save("./examples/figures/lineplot_basic_markers.png", None));
}