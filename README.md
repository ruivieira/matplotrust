![https://crates.io/crates/matplotrust](https://img.shields.io/crates/v/matplotrust.svg) ![https://docs.rs/matplotrust/](https://docs.rs/matplotrust/badge.svg) ![](https://gitlab.com/ruivieira/matplotrust/badges/master/build.svg
)
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/ruivieira%2Fmatplotrust/master)
# matplotrust

A simple library to call Python's `matplotlib`.

## usage

Add `matplotrust` to your `Cargo.toml` dependencies:

```
[dependencies]
matplotrust = "*"
```

Python (3.x) and `matplotlib` must also be installed.

## features

* Scatter plots
* Line plots
* Histograms
* Vertical, horizontal lines
* Line segments
* Plots as Base64

## examples

Examples are available [here](examples/README.md).